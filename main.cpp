#include <QCoreApplication>
#include "mirrofinder.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MirroFinder finder;
    QLoggingCategory::setFilterRules("*.debug=true");

    finder.start();


    return a.exec();
}
